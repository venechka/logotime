<?php if (!defined('ABSPATH')) die('No direct access allowed'); ?>

<?php
//+++
$args = array();
$args['show_count'] = get_option('woof_show_count', 0);
if ($dynamic_recount == -1) {
  $args['show_count_dynamic'] = get_option('woof_show_count_dynamic', 0);
} else {
  $args['show_count_dynamic'] = $dynamic_recount;
}
$args['hide_dynamic_empty_pos'] = 0;
$args['woof_autosubmit'] = $autosubmit;
//***
$_REQUEST['tax_only'] = $tax_only;
$_REQUEST['tax_exclude'] = $tax_exclude;
$_REQUEST['by_only'] = $by_only;

if (!function_exists('woof_show_btn')) {
  
  function woof_show_btn($autosubmit = 1, $ajax_redraw = 0)
  {
    ?>
      <div class="catalog-filter__right text-right">
          <!--a href="#" class="catalog-filter__reset-btn"><img src="/i/cross.svg" alt="cross">Сбросить фильтры</a-->
          <button id="ResetFilter" type="reset" class="catalog-filter__reset-btn"><img src="<?=get_template_directory_uri()?>/custom-assets/i/cross.svg" alt="cross">Сбросить фильтры</button>
      </div>
    
    <?php
  }
  
}

if (!function_exists('woof_only')) {
  
  function woof_only($key_slug, $type = 'taxonomy')
  {
    
    switch ($type) {
      case 'taxonomy':
        
        if (!empty($_REQUEST['tax_only'])) {
          if (!in_array($key_slug, $_REQUEST['tax_only'])) {
            return FALSE;
          }
        }
        
        if (!empty($_REQUEST['tax_exclude'])) {
          if (in_array($key_slug, $_REQUEST['tax_exclude'])) {
            return FALSE;
          }
        }
        
        break;
      
      case 'item':
        if (!empty($_REQUEST['by_only'])) {
          if (!in_array($key_slug, $_REQUEST['by_only'])) {
            return FALSE;
          }
        }
        break;
    }
    
    
    return TRUE;
  }
  
}

//Sort logic  for shortcode [woof] attr tax_only
if (!function_exists('woof_print_tax')) {
  
  function get_order_by_tax_only($t_order, $t_only)
  {
    $temp_array = array_intersect($t_order, $t_only);
    $i = 0;
    foreach ($temp_array as $key => $val) {
      $t_order[$key] = $t_only[$i];
      $i++;
    }
    return $t_order;
  }
  
}
//***
if (!function_exists('woof_print_tax')) {
  
  function woof_print_tax($taxonomies, $tax_slug, $terms, $exclude_tax_key, $taxonomies_info, $additional_taxes, $woof_settings, $args, $counter)
  {
    
    global $WOOF;
    
    if ($exclude_tax_key == $tax_slug) {
      //$terms = apply_filters('woof_exclude_tax_key', $terms);
      if (empty($terms)) {
        return;
      }
    }
    
    //***
    
    if (!woof_only($tax_slug, 'taxonomy')) {
      return;
    }
    
    //***
    
    
    $args['taxonomy_info'] = $taxonomies_info[$tax_slug];
    $args['tax_slug'] = $tax_slug;
    $args['terms'] = $terms;
    $args['all_terms_hierarchy'] = $taxonomies[$tax_slug];
    $args['additional_taxes'] = $additional_taxes;
 
    //***
    $woof_container_styles = "";
    if ($woof_settings['tax_type'][$tax_slug] == 'radio' or $woof_settings['tax_type'][$tax_slug] == 'checkbox') {
      if ($WOOF->settings['tax_block_height'][$tax_slug] > 0) {
        $woof_container_styles = "max-height:{$WOOF->settings['tax_block_height'][$tax_slug]}px; overflow-y: auto;";
      }
    }
    //***
    //https://wordpress.org/support/topic/adding-classes-woof_container-div
    
    ?>
      <div class="catalog-filter__col">
        <?php
        echo $WOOF->render_html(apply_filters('woof_html_types_view_select', WOOF_PATH . 'views/html_types/select.php'), $args);
        ?>
      </div>
    <?php
  }
  
}

if (!function_exists('woof_print_item_by_key')) {
  
  function woof_print_item_by_key($key, $woof_settings, $additional_taxes)
  {
    
    if (!woof_only($key, 'item')) {
      return;
    }
    
    //***
    
    global $WOOF;
    switch ($key) {
      case 'by_price':
        $price_filter = 0;
        if (isset($WOOF->settings['by_price']['show'])) {
          $price_filter = (int)$WOOF->settings['by_price']['show'];
        }
        $tooltip_text = "";
        if (isset($WOOF->settings['by_price']['tooltip_text'])) {
          $tooltip_text = $WOOF->settings['by_price']['tooltip_text'];
        }
        ?>
        
        <?php if ($price_filter == 1): ?>
          <div data-css-class="woof_price_search_container"
               class="woof_price_search_container woof_container woof_price_filter">
              <div class="woof_container_overlay_item"></div>
              <div class="woof_container_inner">
                  <div class="woocommerce widget_price_filter">
                    <?php //the_widget('WC_Widget_Price_Filter', array('title' => ''));        ?>
                    <?php if (isset($WOOF->settings['by_price']['title_text']) and !empty($WOOF->settings['by_price']['title_text'])): ?>
                      <<?php echo apply_filters('woof_title_tag', 'h4'); ?>>
                    <?php echo WOOF_HELPER::wpml_translate(null, $WOOF->settings['by_price']['title_text']); ?>
                    <?php echo WOOF_HELPER::draw_tooltipe(WOOF_HELPER::wpml_translate(null, $WOOF->settings['by_price']['title_text']), $tooltip_text) ?>
                  </<?php echo apply_filters('woof_title_tag', 'h4'); ?>>
                <?php endif; ?>
                <?php WOOF_HELPER::price_filter($additional_taxes); ?>
              </div>
          </div>
          </div>
          <div style="clear:both;"></div>
      <?php endif; ?>
        
        <?php if ($price_filter == 2): ?>
          <div data-css-class="woof_price2_search_container"
               class="woof_price2_search_container woof_container woof_price_filter">
              <div class="woof_container_overlay_item"></div>
              <div class="woof_container_inner">
                <?php if (isset($WOOF->settings['by_price']['title_text']) and !empty($WOOF->settings['by_price']['title_text'])): ?>
                  <<?php echo apply_filters('woof_title_tag', 'h4'); ?>>
                <?php echo WOOF_HELPER::wpml_translate(null, $WOOF->settings['by_price']['title_text']); ?>
                <?php echo WOOF_HELPER::draw_tooltipe(WOOF_HELPER::wpml_translate(null, $WOOF->settings['by_price']['title_text']), $tooltip_text) ?>
              </<?php echo apply_filters('woof_title_tag', 'h4'); ?>>
            <?php endif; ?>
            
            <?php echo do_shortcode('[woof_price_filter type="select" additional_taxes="' . $additional_taxes . '"]'); ?>

          </div>
          </div>
      <?php endif; ?>
        
        
        <?php if ($price_filter == 3): ?>
          <div data-css-class="woof_price3_search_container"
               class="woof_price3_search_container woof_container woof_price_filter">
              <div class="woof_container_overlay_item"></div>
              <div class="woof_container_inner">
                <?php if (isset($WOOF->settings['by_price']['title_text']) and !empty($WOOF->settings['by_price']['title_text'])): ?>
                  <<?php echo apply_filters('woof_title_tag', 'h4'); ?>>
                <?php echo WOOF_HELPER::wpml_translate(null, $WOOF->settings['by_price']['title_text']); ?>
                <?php echo WOOF_HELPER::draw_tooltipe(WOOF_HELPER::wpml_translate(null, $WOOF->settings['by_price']['title_text']), $tooltip_text) ?>
              </<?php echo apply_filters('woof_title_tag', 'h4'); ?>>
            <?php endif; ?>
            
            <?php echo do_shortcode('[woof_price_filter type="slider" additional_taxes="' . $additional_taxes . '"]'); ?>

          </div>
          </div>
      <?php endif; ?>
        
        
        <?php if ($price_filter == 4): ?>
          <div data-css-class="woof_price4_search_container"
               class="woof_price4_search_container woof_container woof_price_filter">
              <div class="woof_container_overlay_item"></div>
              <div class="woof_container_inner">
                <?php if (isset($WOOF->settings['by_price']['title_text']) and !empty($WOOF->settings['by_price']['title_text'])): ?>
                  <<?php echo apply_filters('woof_title_tag', 'h4'); ?>>
                <?php echo WOOF_HELPER::wpml_translate(null, $WOOF->settings['by_price']['title_text']); ?>
                <?php echo WOOF_HELPER::draw_tooltipe(WOOF_HELPER::wpml_translate(null, $WOOF->settings['by_price']['title_text']), $tooltip_text) ?>
              </<?php echo apply_filters('woof_title_tag', 'h4'); ?>>
            <?php endif; ?>
            
            <?php echo do_shortcode('[woof_price_filter type="text" additional_taxes="' . $additional_taxes . '"]'); ?>

          </div>
          </div>
      <?php endif; ?>
        <?php if ($price_filter == 5): ?>
          <div data-css-class="woof_price5_search_container"
               class="woof_price5_search_container woof_container woof_price_filter">
              <div class="woof_container_overlay_item"></div>
              <div class="woof_container_inner">
                <?php
                $css_classes = "woof_block_html_items";
                $show_toggle = 0;
                if (isset($WOOF->settings[$key]['show_toggle_button'])) {
                  $show_toggle = (int)$WOOF->settings[$key]['show_toggle_button'];
                }
                $tooltip_text = "";
                if (isset($WOOF->settings['tooltip_text'][$key])) {
                  $tooltip_text = $WOOF->settings['tooltip_text'][$key];
                }
                //***
                $search_query = $WOOF->get_request_data();
                $block_is_closed = true;
                if (in_array("min_price", array_keys($search_query))) {
                  $block_is_closed = false;
                }
                if ($show_toggle === 1 and !in_array("min_price", array_keys($search_query))) {
                  $css_classes .= " woof_closed_block";
                }
                
                if ($show_toggle === 2 and !in_array("min_price", array_keys($search_query))) {
                  $block_is_closed = false;
                }
                
                if (in_array($show_toggle, array(1, 2))) {
                  $block_is_closed = apply_filters('woof_block_toggle_state', $block_is_closed);
                  if ($block_is_closed) {
                    $css_classes .= " woof_closed_block";
                  } else {
                    $css_classes = str_replace('woof_closed_block', '', $css_classes);
                  }
                }
                ?>
                <?php if (isset($WOOF->settings['by_price']['title_text']) and !empty($WOOF->settings['by_price']['title_text'])): ?>
                  <<?php echo apply_filters('woof_title_tag', 'h4'); ?>>
                <?php echo WOOF_HELPER::wpml_translate(null, $WOOF->settings['by_price']['title_text']); ?>
                <?php WOOF_HELPER::draw_title_toggle($show_toggle, $block_is_closed); ?>
              </<?php echo apply_filters('woof_title_tag', 'h4'); ?>>
            <?php endif; ?>
              <div class="<?php echo $css_classes ?>"
                   <?php if (!empty($woof_container_styles)): ?>style="<?php echo $woof_container_styles ?>"<?php endif; ?>>
                <?php echo do_shortcode('[woof_price_filter type="radio" additional_taxes="' . $additional_taxes . '"]'); ?>
              </div>

          </div>
          </div>
      <?php endif; ?>
        
        <?php
        break;
      
      default:
        do_action('woof_print_html_type_' . $key);
        break;
    }
  }
  
}
?>

    <div class="woof <?php if (!empty($sid)): ?>woof_sid woof_sid_<?php echo $sid ?><?php endif; ?>"
         <?php if (!empty($sid)): ?>data-sid="<?php echo $sid; ?>"<?php endif; ?>
         data-shortcode="<?php echo(isset($_REQUEST['woof_shortcode_txt']) ? $_REQUEST['woof_shortcode_txt'] : 'woof') ?>"
         data-redirect="<?php echo $redirect ?>" data-autosubmit="<?php echo $autosubmit ?>"
         data-ajax-redraw="<?php echo $ajax_redraw ?>">
      
      <?php if ($show_woof_edit_view and !empty($sid)): ?>
          <a href="#" class="woof_edit_view"
             data-sid="<?php echo $sid ?>"><?php _e('show blocks helper', 'woocommerce-products-filter') ?></a>
          <div></div>
      <?php endif; ?>
      <?
      global $wp;
      $action = home_url( $wp->request );
      ?>
        <form id="FilterForm" class="catalog-filter" method="get" action="<?=$action?>">
            <div class="catalog-filter__left">
                <div class="catalog-filter__row">
                    <!--- here is possible to drop html code which is never redraws by AJAX ---->
                  <?php echo apply_filters('woof_print_content_before_redraw_zone', '') ?>

                    <div class="woof_redraw_zone" data-woof-ver="<?php echo WOOF_VERSION ?>">
                      <?php echo apply_filters('woof_print_content_before_search_form', '') ?>
                      <?php
                      if (isset($start_filtering_btn) and (int)$start_filtering_btn == 1) {
                        $start_filtering_btn = true;
                      } else {
                        $start_filtering_btn = false;
                      }
                      
                      if (is_ajax()) {
                        $start_filtering_btn = false;
                      }
                      
                      if ($this->is_isset_in_request_data($this->get_swoof_search_slug())) {
                        $start_filtering_btn = false;
                      }
                      ?>
                      
                      <?php if ($start_filtering_btn): ?>
                          <a href="#"
                             class="woof_button woof_start_filtering_btn"><?php echo $woof_start_filtering_btn_txt ?></a>
                      <?php else: ?>
                        <?php
                        if ($btn_position == 't' or $btn_position == 'tb' or $btn_position == 'bt') {
                          woof_show_btn($autosubmit, $ajax_redraw);
                        }
                        global $wp_query;
                        //+++
                        //if (!empty($taxonomies))
                        {
                          $exclude_tax_key = '';
                          //code-bone for pages like
                          //http://dev.pluginus.net/product-category/clothing/ with GET params
                          //another way when GET is actual no possibility get current taxonomy
                          if ($this->is_really_current_term_exists()) {
                            $o = $this->get_really_current_term();
                            $exclude_tax_key = $o->taxonomy;
                            //do_shortcode("[woof_products_ids_prediction taxonomies=product_cat:{$o->term_id}]");
                            //echo $o->term_id;exit;
                          }
                          //***
                          if (!empty($wp_query->query)) {
                            if (isset($wp_query->query_vars['taxonomy']) and in_array($wp_query->query_vars['taxonomy'], get_object_taxonomies('product'))) {
                              $taxes = $wp_query->query;
                              if (isset($taxes['paged'])) {
                                unset($taxes['paged']);
                              }
                              
                              foreach ($taxes as $key => $value) {
                                if (in_array($key, array_keys($this->get_request_data()))) {
                                  unset($taxes[$key]);
                                }
                              }
                              //***
                              if (!empty($taxes)) {
                                $t = array_keys($taxes);
                                $v = array_values($taxes);
                                //***
                                $exclude_tax_key = $t[0];
                                $_REQUEST['WOOF_IS_TAX_PAGE'] = $exclude_tax_key;
                              }
                            }
                          } else {
                            //***
                          }
                          
                          //***
                          //price sorting
                          ?>
                          <div class="catalog-filter__select" data-select="price">
                            <button type="button" class="catalog-filter__select-btn" data-select="price">Цена</button>
                            <ul class="catalog-filter__select-list">
                                <li>
                                    <button type="button" class="catalog-filter__select-list-btn" data-select="price" data-value="price">По возрастанию</button>
                                </li>
                                <li>
                                    <button type="button" class="catalog-filter__select-list-btn" data-select="price" data-value="price-desc">По убыванию</button>
                                </li>
                            </ul>
                            <input type="hidden" name="orderby" value="<?=$_GET['orderby'] ? $_GET['orderby'] : '0'?>" class="catalog-filter__select-actual" data-select="price">
                          </div>
                          <?
                          $items_order = array();
  
                          $taxonomies_keys = array_keys($taxonomies);
                          if (isset($woof_settings['items_order']) and !empty($woof_settings['items_order'])) {
                            $items_order = explode(',', $woof_settings['items_order']);
                          } else {
                            $items_order = array_merge($this->items_keys, $taxonomies_keys);
                          }
                          
                          //*** lets check if we have new taxonomies added in woocommerce or new item
                          foreach (array_merge($this->items_keys, $taxonomies_keys) as $key) {
                            if (!in_array($key, $items_order)) {
                              $items_order[] = $key;
                            }
                          }
                          
                          //lets print our items and taxonomies
                          $counter = 0;
                          
                          if (count($tax_only) > 0) {
                            $items_order = get_order_by_tax_only($items_order, $tax_only);
                          }
                          
                          if (isset($by_step)) {
                            $new_items_order = explode(',', $by_step);
                            $items_order = array_map('trim', $new_items_order);
                          }
                          
                          foreach ($items_order as $key) {
                            
                            if (in_array($key, $this->items_keys)) {
                              woof_print_item_by_key($key, $woof_settings, $additional_taxes);
                            } else {
                              if (!isset($woof_settings['tax'][$key])) {
                                continue;
                              }
                              
                              woof_print_tax($taxonomies, $key, $taxonomies[$key], $exclude_tax_key, $taxonomies_info, $additional_taxes, $woof_settings, $args, $counter);
                            }
                            $counter++;
                          }
                        }
                        ?>
                      
                      <?php endif; ?>


                    </div>
                </div>
            </div>
            <input type="hidden" name="swoof" value="1">
            <?
                if (!$start_filtering_btn)
                  woof_show_btn($autosubmit, $ajax_redraw);
            ?>
        </form>
    </div>


<?php if ($autohide): ?>
    </div>
    </div>

    </div>
<?php endif; ?>