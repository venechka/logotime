<?php 

function avados_dynamic_css() {

global $avadosFramework;
	
$theme_color_schemes = $avadosFramework['theme-color-schemes'];	
	
$p_color = $avadosFramework['body-text-color'];

$sub_header_link_color = $avadosFramework['sub-header-link-color'];

$sub_header_border_color = $avadosFramework['top-border-bg'];


$sub_header_p_color = $avadosFramework['sub-header-text-color'];

$link_color = $avadosFramework['body-link-color'];

$body_link_hover_color = $avadosFramework['body-link-hover-color'];

$body_background = $avadosFramework['body-background'];

$breadcrumbs_background = $avadosFramework['title-area-background'];




$button_background = $avadosFramework['button-bg'];
$button_text_color = $avadosFramework['btn-text-color'];

$button_top_bottom_padding = $avadosFramework['btn-top-bottom-padding'];
$button_left_right_padding = $avadosFramework['btn-left-right-padding'];


$button_border_color_top = $avadosFramework['btn-border-color-top'];
$button_border_top = $avadosFramework['btn-border-top'];

$button_border_color_right = $avadosFramework['btn-border-color-right'];
$button_border_right = $avadosFramework['btn-border-right'];


$button_border_color = $avadosFramework['btn-border-color'];
$button_border_bottom = $avadosFramework['btn-border-bottom'];

$button_border_color_left = $avadosFramework['btn-border-color-left'];
$button_border_left = $avadosFramework['btn-border-left'];

$button_background_hover = $avadosFramework['button-bg-hover'];
$button_text_color_hover = $avadosFramework['btn-text-color-hover'];

$button_border_top_right_radius = $avadosFramework['btn-border-top-right-radius'];
$button_border_bottom_right_radius = $avadosFramework['btn-border-bottom-right-radius'];
$button_border_bottom_left_radius = $avadosFramework['btn-border-bottom-left-radius'];
$button_border_top_left_radius = $avadosFramework['btn-border-top-left-radius'];

/*border color hover*/
$button_border_cover_hover = $avadosFramework['btn-border-color-hover'];

/*menu colors */
$menu_color = $avadosFramework['menu-color'];




		/* ============================================================
		   ACCENT COLOR
		/* ============================================================*/
	
	echo '<style>';
	
		echo '
		
		
		
.header a {
color: '. $menu_color .';
}
		
		
		
	.btn,	
.service3 .wpb_single_image .vc_figure-caption,
.service3 .service3-title,
.bg-color,
.color-bg,
th,
.line,
.ball-pulse-color > div,
.wpcf7-submit,
.navigation .current,
.tagcloud a:hover,
.woocommerce .widget_price_filter .ui-slider .ui-slider-range,
 .wcmenucart-contents .shop-count,
.ribbon-block,
.woocommerce nav.woocommerce-pagination ul li a:focus, 
.woocommerce nav.woocommerce-pagination ul li a:hover, 
.woocommerce nav.woocommerce-pagination ul li span.current,
#popular-posts li:before,
.woocommerce span.onsale,
.cf7mls_progress_bar li.active:before,
.footer .widget-title:after{
background: '. $theme_color_schemes .';
}
		
a,
a:hover,
		.color,
		.car_repair_i,
		.sidebar ul li a:hover,
		.single-post-style ul li:before,
		.contact-icon,
		.woocommerce .star-rating span,
.woocommerce-message::before,
.subscribe i,
.woocommerce-info:before,
.sidebar ul li a:before,
.color,
.contact-icon,
.woocommerce-tabs ul li:before,
.btn.btn-color-border,
.btn.btn-color-border:hover,
a.btn.btn-color-border:hover
{
			color: '. $theme_color_schemes .';
		}
		
		.border-color,
span.vc_sep_line,
.border-color ul li,
.service3 .wpb_single_image .vc_figure-caption:before,
.service3 .service3-title:before,
.woocommerce-message,
.scrollMenu .current,
ul.circle li:before,
.footer ul li:before,
.sidebar ul.menu li.current-menu-item a,
.widget-title:before,
.title-page:before,
    .blog-category ul.bloglist h3:before,
    .woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
    .ribbon-block:after,
    .woocommerce-info,
    .woocommerce nav.woocommerce-pagination ul li a:focus, 
.woocommerce nav.woocommerce-pagination ul li a:hover, 
.woocommerce nav.woocommerce-pagination ul li span.current,
.uptitle:before,
.ul-line ul li:before,
.wpb_text_column ul li:before,
div.vc_custom_heading:first-child:before,
.uptitle:before,
.woocommerce div.product .woocommerce-tabs ul.tabs li a:hover, 
.woocommerce div.product .woocommerce-tabs ul.tabs li a.selected, 
.woocommerce div.product .woocommerce-tabs ul.tabs li.active a,
.tagcloud a:hover,
.btn.btn-color-border,
.bottom-line-style ul>li>a:after,
.sidebar ul.menu li.current-menu-item,
input:focus, 
textarea:focus, 
select:focus
{
	    border-color:'. $theme_color_schemes .';	
}

		.page-header {
		background: '. $breadcrumbs_background .';
		}
		
body:not(.page-template-page-for-visual-composer) {
		background: '. $body_background .';
		}   
		
		
		.sub_header p,
		.sub_header .fa {
			color: '. $sub_header_p_color .';
		}
		.sub_header a {
			color: '. $sub_header_link_color .';
		}
		.sub_header  {
			border-color: '. $sub_header_border_color .';
		}
		
		
		p,
		.fullpage li,
		.page-content li,
		.sidebar li{
			color: '. $p_color .';
		}
		
		a {
		color: '. $link_color .';
		}
		
		a:hover {
		color: '. $body_link_hover_color .';
		}
		
		
		
		
		
button,		
input[type="submit"],		
.btn, 
.btn:hover,
.form-submit .submit, 
.form-submit .submit:hover,
.woocommerce #respond input#submit, 
.woocommerce a.button, 
.woocommerce button.button, 
.woocommerce input.button,
.woocommerce-product-search input[type="submit"],
.woocommerce a.button:hover,
.woocommerce #respond input#submit.alt, 
.woocommerce a.button.alt,
.woocommerce button.button.alt, 
.woocommerce input.button.alt{
			background: '. $button_background .';
			color: '. $button_text_color .'; 
			padding-top: '. $button_top_bottom_padding .'px;
			padding-bottom: '. $button_top_bottom_padding .'px;
			
			padding-left: '. $button_left_right_padding .'px;
			padding-right: '. $button_left_right_padding .'px;
			
			border-top-color:  '. $button_border_color_top .';
			border-top-width:  '. $button_border_top .'px;
			
			
			border-right-color:  '. $button_border_color_right .';
			border-right-width:  '. $button_border_right .'px;
			
			
			border-bottom-color:  '. $button_border_color .';
			border-bottom-width:  '. $button_border_bottom .'px;
			 
			 border-left-color:  '. $button_border_color_left .';
			border-left-width:  '. $button_border_left .'px;
			 
			border-top-right-radius:  '. $button_border_top_right_radius .'px;
			border-bottom-right-radius:  '. $button_border_bottom_right_radius .'px;
			border-bottom-left-radius:  '. $button_border_bottom_left_radius .'px;
			border-top-left-radius:  '. $button_border_top_left_radius .'px;

			
			}


button:hover,
.btn:hover,
.form-submit .submit:hover,
.woocommerce #respond input#submit:hover, 
.woocommerce a.button:hover, 
.woocommerce button.button:hover, 
.woocommerce input.button:hover,
.woocommerce #respond input#submit.alt:hover, 
.woocommerce a.button.alt:hover, 
.woocommerce button.button.alt:hover, 
.woocommerce input.button.alt:hover{
			background: '. $button_background_hover .';
			color: '. $button_text_color_hover .'; 
			border-color: '. $button_border_cover_hover .';
			
			}




		';
	
	echo '</style>';
	
}

add_action('wp_footer', 'avados_dynamic_css');

?>