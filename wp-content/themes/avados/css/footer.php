<?php
global $avadosFramework;		
?>

<div class="clear"></div>



<div class="footer">
<div class="row">
<div class="container">



<!---------------------------------------
-----------------------------------------
            Breadcrumbs Start 
<!---------------------------------------
----------------------------------------->

<?php 
if( is_front_page() ){ 
?>


<?php } else { ?>

<?php

if ( 
$avadosFramework['breadcrumbs_style'] == 'breadcrumbs_style_footer' 
) {
	get_template_part( '/inc/breadcrumbs/' . $avadosFramework['breadcrumbs_style'] 
	);
}

?>

<?php } ?>


<!---------------------------------------
-----------------------------------------
            Breadcrumbs Finish 
<!---------------------------------------
----------------------------------------->

  <?php if ( is_active_sidebar( 'footer-4' ) ){ ?>
		<div class="text-center">
			<?php dynamic_sidebar( 'footer-4' ); ?>
		</div>
	<?php } ?>
	
	
	
<div class="clear"></div>




<div class="row flex text-center">
  <?php if ( is_active_sidebar( 'footer-1' ) ){ ?>
		<div class="col-3">
			<?php dynamic_sidebar( 'footer-1' ); ?>
		</div>
	<?php } ?>
	
	
	  <?php if ( is_active_sidebar( 'footer-2' ) ){ ?>
		<div class="col-3">
			<?php dynamic_sidebar( 'footer-2' ); ?>
		</div>
	<?php } ?>
	
	  <?php if ( is_active_sidebar( 'footer-3' ) ){ ?>
		<div class="col-3">
			<?php dynamic_sidebar( 'footer-3' ); ?>
          <div class="social">
          	<ul class="social_icon_list">
				<li><a href="https://vk.com/logo_time" target="_blanck"><i class="fab fa-vk"></i></a></li>
				<li><a href="https://instagram.com/logotime5?utm_source=ig_profile_share&igshid=uddxpx0m4buk" target="_blanck"><i class="fab fa-instagram"></i></a></li>
				<li><a href="https://www.facebook.com/LogotimeProduction" target="_blanck"><i class="fab fa-facebook-f"></i></a></li>
			</ul>
          </div>
		</div>
	<?php } ?>




</div><!--/row-->
</div><!--/container-->

</div><!--/footer-->


<?php if ( isset( $avadosFramework['disable_footer_bottom'] ) && $avadosFramework['disable_footer_bottom'] == 1 ) { ?>
<div class="footer-copyright" id="footer">
  <div class="container">
    <div class="row">
      <?php if ( isset( $avadosFramework['disable_footer_copyright'] ) && $avadosFramework['disable_footer_copyright'] == 1 ) { ?>
      <div class="col-2">
        <div class="copyright">
          <!-- Site Copyright -->
          <div class="copyright col_12 last">
            <p><?php echo esc_attr($avadosFramework['footer_bottom_text']); ?></p>
          </div>
          <!-- Site Copyright End -->
        </div> 
      </div>
      <?php } ?>
      <div class="col-2">
        <?php if ( isset( $avadosFramework['disable_footer_bottom_social'] ) && $avadosFramework['disable_footer_bottom_social'] == 1 ) { ?>
        <ul class="social-network social-circle">
          <li> 
            <a href="<?php echo esc_url($avadosFramework['footer_bottom_social_facebook']); ?>" target="_blank"><i class='fa fa-facebook'></i>
            </a>
          </li>
          <li>
            <a href="<?php echo esc_url($avadosFramework['footer_bottom_social_twitter']); ?>" target="_blank"><i class='fa fa-twitter'></i>
            </a>
          </li>
          <li>
            <a href="<?php echo esc_url($avadosFramework['footer_bottom_social_google']); ?>" target="_blank"><i class='fa fa-google-plus'></i>
            </a>
          </li>
        </ul>
        <?php 
}
?>
      </div>
      <div class="clear">
      </div>
    </div>
  </div>
</div>
<?php 
}
?>
<?php if ( isset( $avadosFramework['totop_show'] ) && $avadosFramework['totop_show'] == 1 ) { ?>
<a href="#top">
  <p id="back-top">
    <i class="fa fa-chevron-up">
    </i>
  </p>
</a>
<?php } ?>


</div> <!-- wrapp end -->



<!--noindex-->
<div id="order" class="white-popup-block mfp-hide">

<?php 

$output = do_shortcode($avadosFramework['header-cta-shortcode']);
echo $output;

?>



</div>
<!--/noindex-->

 <div id="fullscreensearch" class="menu-search">
	

	
	<form role="search" method="get" class="searchform" action="' . home_url( '/' ) . '" >
    
    <div class="search-table">
   
    <div class="search-field"><input type="text" placeholder="Type to search" value="" name="s" id="s" /></div>
    
    <button type="submit" class="menu-search-submit"><i class="fa fa-search"></i></button>
    
    </div>
    
    </form>
	</div>


<?php wp_footer();?>

</body>
</html>