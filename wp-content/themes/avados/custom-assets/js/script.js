(function(){
  var $ = jQuery;
  $(document).ready(function() {
    //catalog
    $('.catalog-filter__select-list-btn').on('click', function() {
      var select = $(this).data('select');
      var value = $(this).data('value');
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $('.catalog-filter__select[data-select="'+select+'"]').removeClass('active');
        $('.catalog-filter__select-actual[data-select="'+select+'"]')[0].value = "0";
      } 
      else
      {
        $('.catalog-filter__select[data-select="'+select+'"]').addClass('active');
        $('.catalog-filter__select[data-select="'+select+'"] .catalog-filter__select-list-btn').removeClass('active');
        $(this).addClass('active');
        $('.catalog-filter__select-actual[data-select="'+select+'"]')[0].value = value;
      }
      $('.catalog-filter__select-actual').each(function () {
        if ($(this).val() === '0')
          $(this).remove();
      })
      $('#FilterForm').submit();
    });
    $('#ResetFilter').click(function () {
      $('.catalog-filter__select-actual').remove();
      $('#FilterForm').submit();
    });
    $('.catalog-filter__select-btn').on('click', function(){
      $(this).toggleClass('catalog-filter__select-btn--line');
      $(this).next().toggleClass('catalog-filter__select-list--open');
    });

    //detail
    $('[data-fancybox="gallery-watch-detail"]').fancybox({
      buttons: [
        "zoom",
        //"share",
        //"slideShow",
        //"fullScreen",
        //"download",
        //"thumbs",
        "close"
      ]
    });
    /*
    $(".watch-variants__slider").owlCarousel({
      responsive: {
        // breakpoint from 0 up
        0 : {
          items: 1,
        },
        560 : {
          items: 2,
        },
        992 : {
          items: 3,
        }
      }
    });
    $('[data-owl-prev]').on('click', function(){
      var slider = $(this).data('owlPrev');
      $(slider).trigger('prev.owl.carousel');
    });
    $('[data-owl-next]').on('click', function(){
      var slider = $(this).data('owlNext');
      $(slider).trigger('next.owl.carousel');
    });*/
  });
})()