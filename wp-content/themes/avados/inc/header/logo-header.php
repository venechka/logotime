<?php global $avadosFramework; ?>
<div class="flex"> 
<!-- start logo -->
<div class="max-logo-width"> 
<?php if (function_exists('has_custom_logo') && has_custom_logo()) {
the_custom_logo(); ?>
 <?php } else { ?>
<a href="<?php echo home_url(); ?>" class="flex">  
<?php  if(!empty($avadosFramework['header-logo']['url'])) { ?>
             <img class="logo-main alignleft" src="<?php echo esc_url($avadosFramework['header-logo']['url']); ?>" alt="" style="max-width: <?php echo  $avadosFramework['header-logo-height']; ?>px"/>
<?php } ?>
<?php  if(!empty($avadosFramework['header-logo-light']['url'])) { ?>
             <img class="logo-alt" src="<?php echo esc_url($avadosFramework['header-logo-light']['url']); ?>" alt=""/>
 <?php } ?>    
<?php if ( isset( $avadosFramework['show_site_title_desc'] ) && $avadosFramework['show_site_title_desc'] == 1 ) { ?>
<div class="default-site-logo">  
          <span class="default-site-title">
            <a href="<?php echo esc_url( home_url() ); ?>" title="<?php bloginfo('name'); ?>">
              <?php bloginfo('name'); ?>
            </a>
          </span>
          <span class="default-site-desc">
            <?php bloginfo('description'); ?>
          </span>
        </div>
        
<?php } ?>
 </a>
<?php         
}      
?>
</div>
<!-- finish logo -->
</div>