<?php
/*
Template Name: Настенные часы
Template Post Type: post, page, product
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Настенные часы</title>
	<!-- lightbox CSS -->
	<link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/lightbox.min.css">
	<!-- Bootstrap Reboot CSS -->
	<link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/bootstrap-reboot.min.css">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/bootstrap.min.css">
	<!-- jquery -->
	<link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/jquery.arcticmodal-0.3.css">
	<!-- Main CSS -->
	<link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/main.css">
	<!-- Media CSS -->
	<link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/media.css">
</head>
<body>
	<header class="header">
		<div class="container">
			<div class="row align-items-center d-sm-flex text-center">
				<div class="col-12 col-sm-4">
					<a href="https://logotime.ru/">
						<img src="<?php bloginfo('template_directory') ?>/img/logo.png" alt="" class="logo">
					</a>
				</div>
				<div class="col-12 col-sm-4 text-center">
					<a href="tel:+74956379306" class="tel">
						+7 (495) 637-93-06
					</a>
				</div>
				<div class="col-12 col-sm-4 text-sm-right">
					<a href="#" class="button">
						Заказать звонок
					</a>
				</div>
			</div>
		</div>
	</header>
	<section class="offer offer_wall">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<h1 class="offer-title">
						Настенные часы с логотипом или символикой на заказ
					</h1>
					<a href="https://logotime.ru/assortiment-i-tsenyi/" class="button offer-btn">
						Узнать подробнее
					</a>
				</div>
			</div>
		</div>
	</section>
	<section class="icons">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-4 text-center">
					<div class="icon">
						<img src="<?php bloginfo('template_directory') ?>/img/t1.png" alt="">
						<h3 class="icon__title">
							Любой тираж
						</h3>
						<p class="icon__text">
							Вы можете заказать любое количество, даже 1 штуку
						</p>
					</div>
				</div>
				<div class="col-12 col-sm-4 text-center">
					<div class="icon">
						<img src="<?php bloginfo('template_directory') ?>/img/t2.png" alt="">
						<h3 class="icon__title">
							Широкий ассортимент
						</h3>
						<p class="icon__text">
							В нашем ассортименте более 100 моделей
						</p>
					</div>
				</div>
				<div class="col-12 col-sm-4 text-center">
					<div class="icon">
						<img src="<?php bloginfo('template_directory') ?>/img/t.png" alt="">
						<h3 class="icon__title">
							Высокое качество
						</h3>
						<p class="icon__text">
							Строгое соблюдение стандартов качества
						</p>
					</div>
				</div>
				<div class="col-12 col-sm-4 text-center">
					<div class="icon">
						<img src="<?php bloginfo('template_directory') ?>/img/t4.png" alt="">
						<h3 class="icon__title">
							Бесплатный дизайн
						</h3>
						<p class="icon__text">
							Мы бесплатно разработаем красивый дизайн
						</p>
					</div>
				</div>
				<div class="col-12 col-sm-4 text-center">
					<div class="icon">
						<img src="<?php bloginfo('template_directory') ?>/img/t52.png" alt="">
						<h3 class="icon__title">
							Брендированная упаковка
						</h3>
						<p class="icon__text">
							Упакуем Ваш заказ в индивидуальную упаковку бесплатно
						</p>
					</div>
				</div>
				<div class="col-12 col-sm-4 text-center">
					<div class="icon">
						<img src="<?php bloginfo('template_directory') ?>/img/t6.png" alt="">
						<h3 class="icon__title">
							Гарантия
						</h3>
						<p class="icon__text">
							Гарантийное обслуживание на базе нашего сервис-центра
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="products">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-4">
					<div class="product text-center">
						<img src="<?php the_field('foto1'); ?>" alt="" class="product__img_wall">
						<h4 class="product__title">
							Часы на основе дерева и стекла
						</h4>
					</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="product text-center">
						<img src="<?php the_field('foto2'); ?>" alt="" class="product__img_wall">
						<h4 class="product__title">
							Часы на основе минерального стекла
						</h4>
					</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="product text-center">
						<img src="<?php the_field('foto3'); ?>" alt="" class="product__img_wall">
						<h4 class="product__title">
							Пластиковые настенные часы
						</h4>
					</div>
				</div>
				<!-- <div class="col-12 col-sm-3">
					<div class="product text-center">
						<img src="<?php bloginfo('template_directory') ?>/img/premium.jpg" alt="">
						<h4 class="product__title">
							Деревянные настенные часы премиум класса
						</h4>
					</div>
				</div> -->
			</div>
			<div class="row">
				<div class="col text-center">
					<a href="https://logotime.ru/assortiment-i-tsenyi/" class="button offer-btn">
						Узнать подробнее
					</a>
				</div>
			</div>
		</div>
	</section>
	<section class="works">
		<div class="container">
			<div class="row">
				<div class="col">
					<h2 class="work-title text-center">
						Наши работы:
					</h2>
				</div>
			</div>
			<div class="row text-center align-items-center">
				<div class="col-12 col-sm-4">
					<img src="<?php the_field('foto4'); ?>" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-4">
					<img src="<?php the_field('foto5'); ?>" alt="" class="work-img work-img_size">
				</div>
				<div class="col-12 col-sm-4">
					<img src="<?php the_field('foto6'); ?>" alt="" class="work-img work-img_size_1">
				</div>
				<div class="col-12 col-sm-4">
					<img src="<?php the_field('foto7'); ?>" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-4">
					<img src="<?php the_field('foto8'); ?>" alt="" class="work-img work-img_size">
				</div>
				<div class="col-12 col-sm-4">
					<img src="<?php the_field('foto9'); ?>" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-4">
					<img src="<?php the_field('foto10'); ?>" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-4">
					<img src="<?php the_field('foto11'); ?>" alt="" class="work-img work-img_size">
				</div>
				<div class="col-12 col-sm-4">
					<img src="<?php the_field('foto12'); ?>" alt="" class="work-img">
				</div>
				<!-- <div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/26.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/28.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/29.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/30.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/31.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/34.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/36.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/38.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/39.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/40.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/41.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/43.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/44.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/45.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/46.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/47.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/48.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/49.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/51.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/52.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/53.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/57.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/55.jpg" alt="" class="work-img">
				</div>
				<div class="col-12 col-sm-3">
					<img src="<?php bloginfo('template_directory') ?>/img/56.jpg" alt="" class="work-img">
				</div> -->
			</div>
		</div>
	</section>
	<section class="map">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-5">
					<div class="left text-center">
						<div class="adress">
							<h3 class="adress__title">
								Адрес:
							</h3>
							<p class="adress__text">
								Москва, ул. Амурская, д. 9/6
							</p>
						</div>
						<div class="map-tel">
							<h3 class="tel-title">
								Телефоны:
							</h3>
							<a href="tel:+74956379306" class="footer-tel adress-tel">
								+7 (495) 637-93-06
							</a>
						</div>
						<div class="work-time">
							<h3 class="time-title">
								Время работы:
							</h3>
							<p class="time-text">
								Понедельник-пятница, <br> 9:00-18:00
							</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-7">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2241.9248028002735!2d37.770539063023975!3d55.81190585162737!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b534ebeb32d7b3%3A0xcf6122638ab41abd!2z0JDQvNGD0YDRgdC60LDRjyDRg9C7LiwgOS82LCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTA3MjQx!5e0!3m2!1sru!2sua!4v1547733779053" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</section>
	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col">
					<ul class="footer-menu d-flex text-center justify-content-center">
						<li class="footer-menu__item">
							<a href="https://logotime.ru/" class="footer-menu__link">
								Главная
							</a>
						</li>
						<li class="footer-menu__item">
							<a href="https://logotime.ru/o-kompanii/" class="footer-menu__link">
								О компании
							</a>
						</li>
						<li class="footer-menu__item">
							<a href="https://logotime.ru/assortiment-i-tsenyi/" class="footer-menu__link">
								Ассортимент и цены
							</a>
						</li>
						<li class="footer-menu__item">
							<a href="https://logotime.ru/nanesenie-logotipa/" class="footer-menu__link">
								Нанесение логотипа
							</a>
						</li>
						<li class="footer-menu__item">
							<a href="https://logotime.ru/portfolio/" class="footer-menu__link">
								Портфолио
							</a>
						</li>
						<li class="footer-menu__item">
							<a href="https://logotime.ru/otzyivyi/" class="footer-menu__link">
								Отзывы
							</a>
						</li>
						<li class="footer-menu__item">
							<a href="https://logotime.ru/kontaktyi/" class="footer-menu__link">
								Контакты
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="row align-items-center">
				<div class="col-12 col-sm-4 text-center">
					<a href="tel:+74956379306" class="footer-tel">
						+7 (495) 637-93-06
					</a>
				</div>
				<div class="col-12 col-sm-4 text-center">
					<img src="https://logotime.ru/wp-content/uploads/2018/11/white-logo.png" alt="Logotime" class="footer-img">
				</div>
				<div class="col-12 col-sm-4 text-center">
					<p class="footer-text">
						город Москва, ул. Амурская, д. 9/6
					</p>
				</div>
			</div>
		</div>
	</footer>

	<?php wp_footer();?>
	
	<!-- JavaScript -->

	<script src="<?php bloginfo('template_directory') ?>/js/jquery-3.3.1.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/jquery.arcticmodal-0.3.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/lightbox.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/main.js"></script>
</body>
</html>