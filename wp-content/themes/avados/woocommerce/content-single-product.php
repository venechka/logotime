<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div class="container container--extended">
    <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper bold italic">
                    <div class="avados_title text-center block-center padding-bottom-20">
                        <div class="pre-line title  text-center block-center">
                 
                            <?=get_term($product->get_category_ids()[0], 'product_cat')->name?>
                       
                        </div>
                    </div>
                    <div class="avados_title text-center block-center">
                        <div class="pre-line title  text-center block-center"><?=$product->name?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="watch-detail">
        <div class="watch-detail__row">
            <div class="watch-detail__col watch-detail__col--left">
                <?do_action( 'woocommerce_before_single_product_summary' );?>
            </div>
            <div class="watch-detail__col watch-detail__col--right">
                <?
        
                do_action( 'woocommerce_product_additional_information', $product );
                the_content();
                ?>
                <div class="padding-top-30">
                    <a href="#" class="btn watch-detail__call-btn">Оставить заявку</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
/**
 * Hook: woocommerce_after_single_product_summary.
 *
 * @hooked woocommerce_output_product_data_tabs - 10
 * @hooked woocommerce_upsell_display - 15
 * @hooked woocommerce_output_related_products - 20
 */
do_action( 'woocommerce_after_single_product_summary' );
?>
