<?php
/**
 * Product Loop End
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-end.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     2.0.0
 */
global $modifiedProducts;
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>


<?php
//groping by category, assuming max 2 category depth
/*
    $categories = [];
    foreach ($modifiedProducts as $product) {
        $categories[$product->get_category_ids()[0]][] = $product;
    }
*/
global $wc_shortcode_attributes;
$parent = get_term_by('slug', $wc_shortcode_attributes['category'], 'product_cat');
$args = array(
  'taxonomy' => 'product_cat',
  'parent' => $parent->term_id,
  'hide_empty' => true,
  'show_uncategorized' => false,
  'menu_order' => 'asc',
);

$rawCategories = get_categories( $args );
$categories = [];
foreach ($rawCategories as $rawCategory) {
    $categories[$rawCategory->cat_ID]["category"] = $rawCategory;
    $categories[$rawCategory]["products"] = [];
}

foreach ($modifiedProducts as $product) {
  $categories[$product->get_category_ids()[0]]["products"][] = $product;
}
//category-products loop
?>

<div class="catalog padding-top-50">

<?
  foreach ($categories as $category) {
      ?>
    <div class="catalog-section">
        <div class="catalog-section-title">
            <?=$category["category"]->name?>
        </div>
        <div class="vc_row wpb_row vc_row-fluid">
        <?foreach ($category["products"] as $product) {?>

          <div class="wpb_column vc_column_container vc_col-sm-4">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">
                  <?
                  $link = get_the_permalink( $product->id );
                  ?>
                    <a href="<?=esc_url( $link )?>" class="catalog-card">
                      <?
                      $imgSrc = wp_get_attachment_image_src($product->get_image_id(), 'full');
                      if ($imgSrc) {
                      ?>
                        <img src="<?=$imgSrc[0]?>" class="catalog-card__img" alt="watch-name">
                      <?} else {?>
                        <img src="<?=wc_placeholder_img_src()?>" class="catalog-card__img" alt="watch-name">
                      <?}?>
                        <div class="catalog-card__name">
                            <?=$product->name?>
                        </div>
                    </a>
                </div>
            </div>
          </div>
        <?}?>
        </div>
    </div>
        <?
  }
    ?>
</div>