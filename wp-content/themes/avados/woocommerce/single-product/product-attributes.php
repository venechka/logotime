<?php
/**
 * Product attributes
 *
 * Used by list_attributes() in the products class.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-attributes.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

if ( ! $product_attributes ) {
	return;
}
?>
<div class="watch-detail__col watch-detail__col--right">
    <ul class="watch-detail__chars">
      <?php foreach ( $product_attributes as $product_attribute_key => $product_attribute ) : ?>
        <li class="watch-detail__chars-item">
            <b><?php echo wp_kses_post( $product_attribute['label'] ); ?>:</b> <?php echo wp_kses_post( $product_attribute['value'] ); ?>
        </li>
      <?php endforeach; ?>
    </ul>
</div>
