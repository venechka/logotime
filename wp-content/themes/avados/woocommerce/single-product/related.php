<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.9.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $related_products ) : ?>
    <div class="padding-top-50 padding-bottom-50 avados_title text-center block-center">
        <div class="pre-line title  text-center block-center">Варианты</div>
    </div>
    <div class="watch-variants">
        <div id="variantsSlider" class="watch-variants__slider">
            <?foreach ($related_products as $product) {
              $link = get_the_permalink( $product->id );
            ?>
            <div class="watch-variants__slider-item">
                <a href="<?=esc_url( $link )?>" class="watch-variant">
                  <?
                  $imgSrc = wp_get_attachment_image_src($product->get_image_id(), 'full');
                  if ($imgSrc) {
                    ?>
                      <img src="<?=$imgSrc[0]?>" class="watch-variant__img" alt="watch-name">
                  <?} else {?>
                      <img src="<?=wc_placeholder_img_src()?>" class="watch-variant__img" alt="watch-name">
                  <?}?>
                    <div class="watch-variant__name">
                        <?=$product->name?>
                    </div>
                </a>
            </div>
            <?}?>
        </div>
        <button type="button" class="watch-variants__slider-prev-btn" data-owl-prev="#variantsSlider"></button>
        <button type="button" class="watch-variants__slider-next-btn"  data-owl-next="#variantsSlider"></button>
      <?
      global $product;
      $link = get_the_permalink( $product->get_category_ids()[0] );
      $cat = get_term($product->get_category_ids()[0], 'product_cat');
      $cat = get_term($cat->parent, 'product_cat');
            ?>
        
        <div class="padding-top-30 padding-bottom-30 text-right" style="border-top: 1px solid #333333;">
            <a href="/<?=$cat->slug?>">Возврат в каталог ></a>
        </div>
    </div>
	<?php
endif;

wp_reset_postdata();
